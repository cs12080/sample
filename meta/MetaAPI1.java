﻿
package sample.meta;

import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import sample.able.DiscomfortindexSensable;
import sample.able.HumiditySensable;
import sample.able.TemperatureSensable;

// 気温センサと湿度センサをマッシュアップして不快指数センサAPIを実現するクラス
public class MetaAPI1 implements DiscomfortindexSensable {

	private final DoubleProperty value = new SimpleDoubleProperty();

	public MetaAPI1(final HumiditySensable hSensor, final TemperatureSensable tSensor) {
		this.value.bind(new DoubleBinding() {
			{
				super.bind(hSensor.valueProperty(), tSensor.valueProperty());
			}

			@Override
			protected double computeValue() {
				final double T = tSensor.valueProperty().getValue();
				final double H = hSensor.valueProperty().getValue();
				return 0.81 * T + 0.01 * H * (0.99 * T - 14.3) + 46.3;
			}
		});
	}

	@Override
	public ReadOnlyDoubleProperty valueProperty() {
		return value;
	}

	@Override
	public double getValue() {
		return value.get();
	}
}
