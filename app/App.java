﻿package sample.app;

import java.io.IOException;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import org.alljoyn.bus.BusException;
import sample.able.DiscomfortindexSensable;
import sample.able.HumiditySensable;
import sample.able.TemperatureSensable;
import sample.able.WindowControllable;
import sample.meta.MetaAPI1;

// アプリ開発者が作成するプログラム
// GUIのデザインはApp.fxmlで定義されている
// アプリ実行時には，フレームワークによってインスタンス化される
public class App extends BorderPane {

	// 不快指数のしきい値を表示・入力するスライドバー
	@FXML
	private Slider thresholdBar;

	// 自動操作モードの オン/オフ を 表示/入力 するラジオボタン
	@FXML
	private RadioButton autoModeButton;

	// 現在の不快指数を表示するテキストフィールド
	@FXML
	private TextField discomforIndexTextField;

	// 現在の窓の状態 opened/closed を表示するテキストフィールド
	@FXML
	private TextField isOpenTextField;

	// 窓の開閉制御を行うためのAPI
	private final WindowControllable window;

	// 不快指数センサのAPI
	private final DiscomfortindexSensable dSensor;

	// 不快指数のしきい値
	private final DoubleProperty threshold = new SimpleDoubleProperty(70);

	// 不快指数センサの値がしきい値を超えているかどうか
	private final BooleanBinding overThreshold;
	
	// 自動操作モードがオンかオフか．オンならばtrue
	private final BooleanProperty autoMode = new SimpleBooleanProperty(false);
	
	// 窓コントローラ，湿度センサ，気温センサのAPIが，フレームワークによってコンストラクタで渡される
	// リソースの切換えなども，フレームワークによってアプリからは透過的に行われる
	public App(
			WindowControllable window,
			HumiditySensable hSensor,
			TemperatureSensable tSensor) throws IOException {

		// GUIパーツの初期化
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("App.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);
		fxmlLoader.load();

		// 窓の制御API
		this.window = window;

		// 不快指数センサAPI（HumiditySensableとTemperatureSensableをマッシュアップしたもの）
		this.dSensor = new MetaAPI1(hSensor, tSensor);

		//窓の開閉状態をGUIとバインド
		this.isOpenTextField.textProperty().bind(Bindings.when(window.isOpenProperty()).then("opened").otherwise("closed"));

		// 不快指数の値をGUIとバインド
		this.discomforIndexTextField.textProperty().bind(this.dSensor.valueProperty().asString());

		// しきい値をGUIとバインド
		this.threshold.bind(this.thresholdBar.valueProperty());

		// 不快指数がしきい値を超えているかどうか
		this.overThreshold = this.threshold.lessThan(this.dSensor.valueProperty());

		// 自動操縦モードの on/off をGUIと双方向バインド
		this.autoMode.bindBidirectional(this.autoModeButton.selectedProperty());

		// 自動操作モードの on/off が切り替わったときの処理
		this.autoMode.addListener((unuse, isAutoOld, isAutoNew) -> {
			openOrClose();
		});

		// 不快指数がしきい値を 超えた/越えなかった という論理式の値が変わった時の処理
		this.overThreshold.addListener((unuse, oldValue, newValue) -> {
			openOrClose();
		});
	}

	// もし自動操作モードがonの場合，不快指数がしきい値を超えていたら窓を開け，超えていなければ窓を閉める
	// マニュアル操作モード（自動操作モードがoff）の場合は何もしない
	private void openOrClose() {
		if (autoMode.get()) {
			if (overThreshold.get()) {
				window.open();
			} else {
				window.close();
			}
		}
	}

	// Openボタンが押されると実行される
	@FXML
	public void windoOpen() throws BusException {
		// 手動操作モードにする
		autoMode.set(false);

		// 窓を開ける
		window.open();
	}

	// Closeボタンが押されると実行される
	@FXML
	public void windoClose() throws BusException {
		// 手動操作モードにする
		autoMode.set(false);

		// 窓を閉める
		window.close();
	}

}
