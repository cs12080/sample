﻿package sample.able;

import javafx.beans.property.ReadOnlyBooleanProperty;

// 窓コントローラAPI（アプリ開発者はこのAPIを利用してアプリを開発する）
public interface WindowControllable {
	
	// 窓の開閉状態のバインド可能な値を取得
	public ReadOnlyBooleanProperty isOpenProperty();
	
	// 窓の開閉状態を取得
	public boolean getIsOpen();
	
	// 窓を開く
	public void open();

	// 窓を閉じる
	public void close();
	
}
