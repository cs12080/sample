﻿package sample.able;

import javafx.beans.property.ReadOnlyDoubleProperty;

// 不快指数センサAPI（アプリ開発者はこのAPIを利用してアプリを開発する）
public interface DiscomfortindexSensable {

	// 現在の不快指数のバインド可能な値を取得
	public ReadOnlyDoubleProperty valueProperty();

	// 現在の不快指数を取得
	public double getValue();
	
}
