﻿package sample.able;

import javafx.beans.property.ReadOnlyDoubleProperty;

// 湿度センサAPI（アプリ開発者はこのAPIを利用してアプリを開発する）
public interface HumiditySensable {
	
	// 現在の湿度のバインド可能な値を取得
	public ReadOnlyDoubleProperty valueProperty();

	// 現在の湿度を取得
	public double getValue();
	
}
