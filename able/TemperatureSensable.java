﻿package sample.able;

import javafx.beans.property.ReadOnlyDoubleProperty;

// 気温センサAPI（アプリ開発者はこのAPIを利用してアプリを開発する）
public interface TemperatureSensable {
	
	// 現在の気温のバインド可能な値を取得
	public ReadOnlyDoubleProperty valueProperty();
	
	// 現在の気温を取得
	public double getValue();
	
}
